import requests

class getWeather:

	data = None
	count = None

	def __init__(self, apiKey, city, unit):
		self.apiKey = apiKey
		self.unit = unit
		self.city = city
		
	def requestForecast(self, apiKey, city, unit, cast = 'forecast'):
		url = f'http://api.openweathermap.org/data/2.5/{cast}?appid={apiKey}&q={city}&mode=json&units={unit}' 

		response = requests.get(url).json()
		self.count = response['cnt']
		self.data = response

		print('New data has been retrieved')
		return response

	def printForecast(self):
		for day in range(self.count):
			date = self.data['list'][day]['dt_txt']
			temp = self.data['list'][day]['main']['temp']
			humidity = self.data['list'][day]['main']['humidity']
			windSpeed = self.data['list'][day]['wind']['speed']
			windDeg = self.data['list'][day]['wind']['deg']
			weather = self.data['list'][day]['weather'][0]['description']
			weatherId = self.data['list'][day]['weather'][0]['id']
			cloud = self.data['list'][day]['clouds']['all']
			
			print('\nDate/time:	{}'.format(date))
			print('Temperature: 	{} Celsius'.format(temp))
			print('Humidity:	{}%'.format(humidity))
			print('windspeed:	{} m/s'.format(windSpeed))
			print('windDeg:	{}'.format(windDeg))
			print('Clouds: 	{}%'.format(cloud))
			print('Weather ID: 	{}'.format(weatherId))
			print('Weather: 	{}'.format(weather))
		
